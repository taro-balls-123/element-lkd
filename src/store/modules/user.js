import { login } from '@/api/login'
import { Message } from 'element-ui'
import { setToken, getToken } from '@/utils/storage'
import router from '@/router'
export default {
  namespaced: true,
  state: {
    token: getToken()
  },
  mutations: {
    setToken (state, val) {
      state.token = val
    }
  },
  /* 处理数据存入仓库 */
  actions: {
    async settoken (context, payload) {
      console.log(payload)
      const res = await login({
        ...payload
      })
      console.log(res)
      if (res.success) {
        context.commit('setToken', res.token)
        setToken(res.token)
        Message.success(res.msg)
        router.push('/')
      } else {
        Message.error(res.msg)
      }
    }
  }
}
