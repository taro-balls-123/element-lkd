import Vue from 'vue'
import VueRouter from 'vue-router'
// 引入组件
import Login from '@/views/login'
import Layout from '@/views/layout'

Vue.use(VueRouter)

// 配置路由规则
const routes = [
  {
    path: '/login',
    component: Login
  },
  {
    path: '/',
    component: Layout
  }

]

const router = new VueRouter({
  routes
})

export default router
