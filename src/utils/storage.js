const KEY1 = 'my-token-element-pc'
const KEY2 = 'my-uname'
// 设置
export const setToken = (newToken) => {
  localStorage.setItem(KEY1, newToken)
}
// 获取
export const getToken = () => {
  return localStorage.getItem(KEY1)
}
// 删除
export const delToken = () => {
  localStorage.removeItem(KEY1)
}

// 存用户名
export const setUname = newUname => {
  localStorage.setItem(KEY2, newUname)
}
export const getUname = () => {
  return localStorage.getItem(KEY2)
}
export const removeUname = () => {
  localStorage.removeItem(KEY2)
}
