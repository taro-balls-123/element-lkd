import request from '@/utils/request'
// 发送乱码请求
export const getCodes = (num) => request({
  method: 'get',
  url: `/api/user-service/user/imageCode/${num}`,
  responseType: 'arraybuffer'
})
